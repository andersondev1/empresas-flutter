import 'package:flutter_test/flutter_test.dart';
import 'package:ioasys_test/core/constants/key_storage.dart';
import 'package:ioasys_test/core/drivers/localstorage/local_storage.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import 'local_storage_test.mocks.dart';

@GenerateMocks([ILocalStorage])
void main() {
  final mockStorage = MockILocalStorage();
  setUp(() {});

  test('test save localStorage', () async {
    when(mockStorage.save('key', 'value')).thenAnswer((_) async {});
  });

  test('test contains key true in localStorage', () async {
    when(await mockStorage.containsKey('key')).thenReturn(true);
  });

  test('test contains key false in localStorage', () async {
    when(await mockStorage.containsKey(KeyStorage.keyHeaders))
        .thenReturn(false);
  });

  test('test get key in localStorage', () async {
    when(mockStorage.getAuth(KeyStorage.keyHeaders))
        .thenAnswer((_) => Future.value());
  });
}
