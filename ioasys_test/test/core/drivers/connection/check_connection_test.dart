import 'package:flutter_test/flutter_test.dart';
import 'package:ioasys_test/core/drivers/connection/check_connection.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import 'check_connection_test.mocks.dart';

@GenerateMocks([Connection])
void main() {
  final mockConnection = MockConnection();
  setUp(() {});
  test('have connection', () async {
    when(await mockConnection.checkConnection()).thenReturn(true);
  });

  test('dont have connection', () async {
    when(await mockConnection.checkConnection()).thenReturn(false);
  });
}
