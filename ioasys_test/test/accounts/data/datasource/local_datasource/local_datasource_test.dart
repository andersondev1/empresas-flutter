import 'package:flutter_test/flutter_test.dart';
import 'package:ioasys_test/accounts/data/datasource/local_datasource.dart';
import 'package:ioasys_test/accounts/data/models/auth_model.dart';
import 'package:ioasys_test/core/drivers/localstorage/local_storage.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import 'local_datasource_test.mocks.dart';

@GenerateMocks([ILocalStorage])
void main() {
  late LocalDataSource remoteDataSource;
  final mockStorage = MockILocalStorage();
  setUp(() {
    remoteDataSource = LocalDataSourceImpl(mockStorage);
  });

  test('save account details', () async {
    when(remoteDataSource.saveAccountDetails(
      AuthModel(
          uid: 'testeapple@ioasys.com.br',
          client: 'nY99H_DOaWYUcMBhcwff9w',
          token: 'N5eQcTJRRdxL7ZeOiOB-FQ'),
    )).thenAnswer((_) async {});
  });
}
