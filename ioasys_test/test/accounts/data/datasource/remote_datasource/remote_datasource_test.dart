import 'package:flutter_test/flutter_test.dart';
import 'package:ioasys_test/accounts/data/datasource/remote_datasource.dart';
import 'package:ioasys_test/accounts/data/models/login_model.dart';
import 'package:ioasys_test/core/constants/key_storage.dart';
import 'package:ioasys_test/core/drivers/dio/dio_client.dart';
import 'package:ioasys_test/core/drivers/localstorage/local_storage.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'remote_datasource_test.mocks.dart';

@GenerateMocks([ILocalStorage])
void main() {
  late RemoteDataSource remoteDataSource;
  late DioClient dioClient;
  late LoginModel loginAccountEntityVerify;
  late LoginModel loginAccountEntityNotVerify;
  final mockStorage = MockILocalStorage();
  setUp(() {
    dioClient = DioClient(mockStorage);
    remoteDataSource = RemoteDataSourceImpl(dioClient);
    loginAccountEntityVerify =
        LoginModel(email: 'testeapple@ioasys.com.br', password: '12341234');
    loginAccountEntityNotVerify =
        LoginModel(email: 'testeappl1e@ioasys.com.br', password: '112341234');
  });

  test('test login in remote datasource when authModel is not null', () async {
    when(mockStorage.getAuth(KeyStorage.keyHeaders))
        .thenAnswer((_) async => Future.value());
    var response =
        await remoteDataSource.loginWithEmail(loginAccountEntityVerify);
    expect(response != null, true);
  });

  test('test login in remote datasource when authModel is null', () async {
    when(mockStorage.getAuth(KeyStorage.keyHeaders))
        .thenAnswer((_) async => Future.value());
    var response =
        await remoteDataSource.loginWithEmail(loginAccountEntityNotVerify);
    expect(response == null, true);
  });
}
