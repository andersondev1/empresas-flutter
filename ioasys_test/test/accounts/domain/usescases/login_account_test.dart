import 'package:flutter_test/flutter_test.dart';
import 'package:ioasys_test/accounts/data/datasource/local_datasource.dart';
import 'package:ioasys_test/accounts/data/datasource/remote_datasource.dart';
import 'package:ioasys_test/accounts/data/models/login_model.dart';
import 'package:ioasys_test/accounts/data/repositories/account_repository_impl.dart';
import 'package:ioasys_test/accounts/domain/repositories/account_repository.dart';
import 'package:ioasys_test/accounts/domain/usescases/login_account_usecase.dart';
import 'package:ioasys_test/core/constants/key_storage.dart';
import 'package:ioasys_test/core/drivers/connection/check_connection.dart';
import 'package:ioasys_test/core/drivers/dio/dio_client.dart';
import 'package:ioasys_test/core/drivers/localstorage/local_storage.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import 'login_account_test.mocks.dart';

@GenerateMocks([ILocalStorage, Connection, DioClient])
void main() {
  late LocalDataSource localDataSource;
  final mockStorage = MockILocalStorage();
  final mockConnection = MockConnection();
  late DioClient dioClient;
  late RemoteDataSource remoteDataSource;
  late LoginAccountUseCase loginAccountUseCase;
  late AccountRepository accountRepository;
  late LoginModel loginAccountEntity;
  setUp(() {
    dioClient = MockDioClient();
    localDataSource = LocalDataSourceImpl(mockStorage);
    remoteDataSource = RemoteDataSourceImpl(dioClient);
    accountRepository = AccountRepositoryImpl(
        localDataSource, remoteDataSource, mockConnection);
    loginAccountUseCase = LoginAccountUseCase(accountRepository);
    loginAccountEntity =
        LoginModel(email: 'testeapple@ioasys.com.br', password: '12341234');
  });

  test('make login repository', () async {
    when(mockConnection.checkConnection())
        .thenAnswer((_) async => Future.value(true));
    when(mockStorage.getAuth(KeyStorage.keyHeaders))
        .thenAnswer((_) async => Future.value());
    var response = await loginAccountUseCase.call(loginAccountEntity);
    response.fold((left) {
      expect(left, '');
    }, (right) {
      expect(right, true);
    });
  });
}
