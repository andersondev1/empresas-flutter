import 'package:flutter_test/flutter_test.dart';
import 'package:ioasys_test/companys/data/datasource/remote_datasource.dart';
import 'package:ioasys_test/companys/data/models/search_enterprises_model.dart';
import 'package:ioasys_test/core/constants/key_storage.dart';
import 'package:ioasys_test/core/drivers/dio/dio_client.dart';
import 'package:ioasys_test/core/drivers/localstorage/local_storage.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'remote_datasource_test.mocks.dart';

@GenerateMocks([ILocalStorage])
void main() {
  late RemoteDataSource remoteDataSource;
  late DioClient dioClient;
  final mockStorage = MockILocalStorage();
  setUp(() {
    dioClient = DioClient(mockStorage);
    remoteDataSource = RemoteDataSourceImpl(dioClient);
  });

  test('test details enterprises in remote datasource', () async {
    when(mockStorage.getAuth(KeyStorage.keyHeaders))
        .thenAnswer((_) async => Future.value());
    when(remoteDataSource.detailsEnterprises(1))
        .thenAnswer((_) async => Future.value());
  });
  test('test fails get details enterprises in remote datasource', () async {
    when(mockStorage.getAuth(KeyStorage.keyHeaders))
        .thenAnswer((_) async => Future.value());
    when(remoteDataSource.detailsEnterprises(1))
        .thenAnswer((_) async => Future.value());
  });
  test('test search enterprises in remote datasource', () async {
    when(mockStorage.getAuth(KeyStorage.keyHeaders))
        .thenAnswer((_) async => Future.value());
    when(remoteDataSource
            .searchEnterprises(SearchEnterprisesModel(id: 1, name: 'name')))
        .thenAnswer((_) async => Future.value());
  });

  test('test fails search enterprises in remote datasource', () async {
    when(mockStorage.getAuth(KeyStorage.keyHeaders))
        .thenAnswer((_) async => Future.value());
    when(remoteDataSource
        .searchEnterprises(SearchEnterprisesModel(id: 1, name: 'name')));
  });
}
