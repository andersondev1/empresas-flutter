import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:ioasys_test/companys/data/datasource/remote_datasource.dart';
import 'package:ioasys_test/companys/data/repositories/enterprises_repository_impl.dart';
import 'package:ioasys_test/companys/domain/entities/search_enterprises_entity.dart';
import 'package:ioasys_test/companys/domain/repositories/enterprises_repository.dart';
import 'package:ioasys_test/core/constants/key_storage.dart';
import 'package:ioasys_test/core/drivers/connection/check_connection.dart';
import 'package:ioasys_test/core/drivers/dio/dio_client.dart';
import 'package:ioasys_test/core/drivers/localstorage/local_storage.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import 'company_repository_impl_test.mocks.dart';

@GenerateMocks([ILocalStorage, Connection])
void main() {
  late EnterprisesRepository enterprisesRepository;
  late RemoteDataSource remoteDataSource;
  late DioClient dioClient;
  final mockStorage = MockILocalStorage();
  final mockConnection = MockConnection();
  setUp(() {
    WidgetsFlutterBinding.ensureInitialized();
    dioClient = DioClient(mockStorage);
    remoteDataSource = RemoteDataSourceImpl(dioClient);
    enterprisesRepository = EnterprisesRepositoryImpl(
      remoteDataSource,
      mockConnection,
    );
  });
  test('get details enterprises repository', () async {
    when(mockConnection.checkConnection())
        .thenAnswer((_) async => Future.value(true));
    when(mockStorage.getAuth(KeyStorage.keyHeaders))
        .thenAnswer((_) async => Future.value());
    var response = await enterprisesRepository.detailsEnterprises(1);
    response.fold((left) {
      expect(left, '');
    }, (right) {
      expect(right, true);
    });
  });

  test('search enterprises repository', () async {
    when(mockConnection.checkConnection())
        .thenAnswer((_) async => Future.value(true));
    when(mockStorage.getAuth(KeyStorage.keyHeaders))
        .thenAnswer((_) async => Future.value());
    var response = await enterprisesRepository.searchEnterprises(
      SearchEnterprisesEntity(
        id: 1,
        name: 'name',
      ),
    );
    response.fold((left) {
      expect(left, '');
    }, (right) {
      expect(right, true);
    });
  });
}
