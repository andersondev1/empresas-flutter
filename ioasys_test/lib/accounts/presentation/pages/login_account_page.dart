import 'package:flutter/material.dart';
import 'package:ioasys_test/accounts/presentation/store/login_account_store.dart';
import 'package:provider/provider.dart';

class LoginAccountPage extends StatefulWidget {
  const LoginAccountPage({Key? key}) : super(key: key);

  @override
  State<LoginAccountPage> createState() => _LoginAccountPageState();
}

class _LoginAccountPageState extends State<LoginAccountPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Consumer<LoginAccountStore>(builder: (context, login, _) {
        if (login.viewStateLoading) {
          return const CircularProgressIndicator();
        }
        return Form(
          child: Column(children: [
            TextFormField(
              controller: login.emailController,
              validator: (value) {
                return null;
              },
            ),
            TextFormField(
              controller: login.passwordController,
              obscureText: true,
              validator: (value) {
                return null;
              },
            ),
            ElevatedButton(
              onPressed: () async {
                await login.login();
                if (login.viewStateError) {
                  ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                    content: Text(login.error),
                  ));
                }
              },
              child: const Text('entrar'),
            )
          ]),
        );
      }),
    );
  }
}
