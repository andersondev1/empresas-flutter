import 'package:flutter/cupertino.dart';
import 'package:get_it/get_it.dart';
import 'package:ioasys_test/accounts/domain/entity/login_account_entity.dart';
import 'package:ioasys_test/accounts/domain/usescases/login_account_usecase.dart';
import 'package:ioasys_test/core/constants/key_routes.dart';
import 'package:ioasys_test/core/routes/navigation_routes.dart';

enum ViewState { isLoading, error, done }

class LoginAccountStore extends ChangeNotifier {
  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();

  String error = '';
  ViewState viewState = ViewState.done;

  bool get viewStateError => viewState == ViewState.error;
  bool get viewStateLoading => viewState == ViewState.isLoading;
  bool get viewStatedone => viewState == ViewState.done;
  Future<void> login() async {
    final LoginAccountUseCase _loginAccount =
        GetIt.I.get<LoginAccountUseCase>();

    viewState = ViewState.isLoading;

    LoginAccountEntity _login = LoginAccountEntity(
        email: emailController.text, password: passwordController.text);
    var response = await _loginAccount.call(_login);

    response.fold((failure) {
      viewState = ViewState.error;
      error = failure.toString();
    }, (success) {
      CustomNavigator()
          .pushNamedAndRemoveUntil(KeyRoutes.homeRoute, (p0) => false);
    });

    viewState = ViewState.done;
    notifyListeners();
  }
}
