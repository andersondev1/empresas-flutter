class AuthEntity {
  final String uid;
  final String client;
  final String token;
  AuthEntity({
    required this.uid,
    required this.client,
    required this.token,
  });
}
