class LoginAccountEntity {
  String email;
  String password;

  LoginAccountEntity({
    required this.email,
    required this.password,
  });
}
