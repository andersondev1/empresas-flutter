import 'package:dartz/dartz.dart';
import 'package:ioasys_test/accounts/domain/entity/login_account_entity.dart';
import 'package:ioasys_test/accounts/domain/repositories/account_repository.dart';
import 'package:ioasys_test/core/exceptions/failure.dart';
import 'package:ioasys_test/core/usecases/use_case.dart';

class LoginAccountUseCase extends UseCase<LoginAccountEntity, bool> {
  final AccountRepository accountRepository;

  LoginAccountUseCase(this.accountRepository);
  @override
  Future<Either<Failure, bool>> call(LoginAccountEntity params) async {
    return await accountRepository.login(params);
  }
}
