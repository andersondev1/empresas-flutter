import 'package:dartz/dartz.dart';
import 'package:ioasys_test/accounts/domain/entity/login_account_entity.dart';
import 'package:ioasys_test/core/exceptions/failure.dart';

abstract class AccountRepository {
  Future<Either<Failure, bool>> login(
    LoginAccountEntity loginAccountEntity,
  );
}
