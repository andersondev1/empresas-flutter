import 'package:ioasys_test/accounts/domain/entity/auth_entity.dart';

class AuthModel extends AuthEntity {
  AuthModel({
    required String uid,
    required String client,
    required String token,
  }) : super(client: client, token: token, uid: uid);

  factory AuthModel.fromJson(Map<String, dynamic> json) {
    return AuthModel(
      uid: json['uid'][0],
      client: json['client'][0],
      token: json['access-token'][0],
    );
  }
  Map<String, dynamic> toJson() {
    return {
      'uid': uid,
      'client': client,
      'access-token': token,
    };
  }
}
