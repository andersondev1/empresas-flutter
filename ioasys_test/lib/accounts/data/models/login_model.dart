import 'package:ioasys_test/accounts/domain/entity/login_account_entity.dart';

class LoginModel extends LoginAccountEntity {
  LoginModel({
    required String email,
    required String password,
  }) : super(
          email: email,
          password: password,
        );

  factory LoginModel.fromEntity(LoginAccountEntity loginAccountEntity) {
    return LoginModel(
      email: loginAccountEntity.email,
      password: loginAccountEntity.password,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'email': email,
      'password': password,
    };
  }
}
