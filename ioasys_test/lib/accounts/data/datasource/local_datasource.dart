import 'dart:convert';

import 'package:ioasys_test/accounts/data/models/auth_model.dart';
import 'package:ioasys_test/core/constants/key_storage.dart';
import 'package:ioasys_test/core/drivers/localstorage/local_storage.dart';

abstract class LocalDataSource {
  Future<void> saveAccountDetails(AuthModel userCredential);
  Future<AuthModel?> getHeaders();
}

class LocalDataSourceImpl extends LocalDataSource {
  final ILocalStorage localStorage;
  LocalDataSourceImpl(this.localStorage);
  @override
  Future<AuthModel?> getHeaders() async {
    return await localStorage.getAuth(KeyStorage.keyHeaders);
  }

  @override
  Future<void> saveAccountDetails(AuthModel userCredential) async {
    await localStorage.save(
        KeyStorage.keyHeaders, jsonEncode(userCredential.toJson()));
  }
}
