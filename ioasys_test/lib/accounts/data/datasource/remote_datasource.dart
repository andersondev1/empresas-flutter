import 'package:ioasys_test/accounts/data/models/auth_model.dart';
import 'package:ioasys_test/accounts/data/models/login_model.dart';
import 'package:ioasys_test/core/drivers/dio/dio_client.dart';

abstract class RemoteDataSource {
  Future<AuthModel?> loginWithEmail(LoginModel loginAccountEntity);
}

class RemoteDataSourceImpl extends RemoteDataSource {
  final DioClient dioClient;
  RemoteDataSourceImpl(this.dioClient);
  @override
  Future<AuthModel> loginWithEmail(LoginModel loginAccountEntity) async {
    final response = await dioClient.post(
      'users/auth/sign_in',
      body: loginAccountEntity.toJson(),
    );
    return AuthModel.fromJson(response.headers.map);
  }
}
