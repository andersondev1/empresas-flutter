import 'package:dio/dio.dart';
import 'package:ioasys_test/accounts/data/datasource/local_datasource.dart';
import 'package:ioasys_test/accounts/data/datasource/remote_datasource.dart';
import 'package:ioasys_test/accounts/data/models/login_model.dart';
import 'package:ioasys_test/accounts/domain/entity/login_account_entity.dart';
import 'package:dartz/dartz.dart';
import 'package:ioasys_test/accounts/domain/repositories/account_repository.dart';
import 'package:ioasys_test/core/drivers/connection/check_connection.dart';
import 'package:ioasys_test/core/exceptions/failure.dart';

class AccountRepositoryImpl extends AccountRepository {
  final LocalDataSource localDataSource;
  final RemoteDataSource remoteDataSource;
  final Connection connection;
  AccountRepositoryImpl(
    this.localDataSource,
    this.remoteDataSource,
    this.connection,
  );
  @override
  Future<Either<Failure, bool>> login(
      LoginAccountEntity loginAccountEntity) async {
    var hasInternet = await connection.checkConnection();
    if (hasInternet) {
      try {
        var response = await remoteDataSource
            .loginWithEmail(LoginModel.fromEntity(loginAccountEntity));
        if (response != null) {
          await localDataSource.saveAccountDetails(response);
          return const Right(true);
        }
        return Left(Failure('try again'));
      } catch (error) {
        if (error is DioError) {
          return Left(Failure(error.message));
        }
      }
      return Left(Failure('Something went wrong'));
    }
    return Left(Failure('no connection'));
  }
}
