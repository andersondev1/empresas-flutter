class Failure implements Exception {
  String failure;
  Failure(this.failure);

  @override
  String toString() {
    return failure;
  }
}
