import 'package:dartz/dartz.dart';

abstract class UseCase<Params, Type> {
  Future<Either<Exception, Type>> call(Params params);
}
