import 'package:flutter/foundation.dart';

class LogUtils {
  static void show(dynamic log) {
    if (kDebugMode) {
      print(log);
    }
  }
}
