import 'dart:convert';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:ioasys_test/accounts/data/models/auth_model.dart';

abstract class ILocalStorage {
  Future<void> save(String key, String? value);
  Future<AuthModel?> getAuth(String key);
  Future<bool> containsKey(String key);
}

class LocalStorage extends ILocalStorage {
  final FlutterSecureStorage storage;

  LocalStorage(this.storage);
  @override
  Future<void> save(String key, String? value) async {
    await storage.write(key: key, value: value);
  }

  @override
  Future<AuthModel?> getAuth(String key) async {
    final token = await storage.read(key: key);
    return token != null ? AuthModel.fromJson(jsonDecode(token)) : null;
  }

  @override
  Future<bool> containsKey(String key) async {
    return await storage.containsKey(key: key);
  }
}
