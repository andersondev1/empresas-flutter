import 'package:dio/dio.dart';
import 'package:ioasys_test/core/constants/key_storage.dart';
import 'package:ioasys_test/core/drivers/localstorage/local_storage.dart';
import 'package:ioasys_test/core/utils/log_utils.dart';

class CustomDioInterceptors extends Interceptor {
  final ILocalStorage localStorage;

  CustomDioInterceptors(this.localStorage);
  @override
  void onRequest(
      RequestOptions options, RequestInterceptorHandler handler) async {
    LogUtils.show('REQUEST[${options.method}] => PATH: ${options.path}');
    final authModel = await localStorage.getAuth(KeyStorage.keyHeaders);

    if (authModel != null) {
      final optionsWithToken = options.copyWith(
        headers: {
          'uid': authModel.uid,
          'client': authModel.client,
          'access-token': 'Bearer ${authModel.token}'
        },
      );

      return super.onRequest(optionsWithToken, handler);
    }

    return super.onRequest(options, handler);
  }

  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) {
    LogUtils.show(
        'RESPONSE[${response.statusCode}] => PATH: ${response.requestOptions.path}');
    return super.onResponse(response, handler);
  }

  @override
  void onError(DioError err, ErrorInterceptorHandler handler) {
    LogUtils.show(
        'ERROR[${err.response?.statusCode}] => PATH: ${err.requestOptions.path} message: ${err.response}');
    return super.onError(err, handler);
  }
}
