import 'dart:io';
import 'package:dio/dio.dart';
import 'package:ioasys_test/core/drivers/dio/dio_interceptors.dart';
import 'package:ioasys_test/core/drivers/localstorage/local_storage.dart';
import 'package:ioasys_test/core/exceptions/failure.dart';

class DioClient {
  late Dio _dio;
  final ILocalStorage localStorage;
  DioClient(this.localStorage) {
    _dio = Dio(
      BaseOptions(
        baseUrl: 'https://empresas.ioasys.com.br/api/v1/',
      ),
    )..interceptors.add(CustomDioInterceptors(localStorage));
  }

  Object _catchExceptions(Object exception) {
    if (exception is DioError) {
      if (exception.response!.statusCode == 401) {
        return Failure('');
      }
      if (exception.error is SocketException) {
        return Failure('');
      }
      return Failure('');
    } else {
      return Failure('');
    }
  }

  Future<Response> get(
    String endpoint, {
    Map<String, dynamic>? query,
  }) async {
    try {
      final response = await _dio.get(endpoint, queryParameters: query);
      return response;
    } catch (e) {
      throw _catchExceptions(e);
    }
  }

  Future<Response> post(
    String endpoint, {
    Map<String, dynamic>? body,
  }) {
    return _dio.post(endpoint, data: body);
  }

  Future<Map<String, dynamic>> patch(
    String endpoint, {
    Map<String, dynamic>? body,
  }) async {
    try {
      final response = await _dio.patch(endpoint, data: body);
      return response.data;
    } catch (e) {
      throw _catchExceptions(e);
    }
  }
}
