import 'package:connectivity_plus/connectivity_plus.dart';

class Connection {
  final Connectivity _connectivity;

  Connection(this._connectivity);

  Future<bool> checkConnection() async {
    var connection = await _connectivity.checkConnectivity();
    return connection != ConnectivityResult.none &&
        connection != ConnectivityResult.bluetooth;
  }
}
