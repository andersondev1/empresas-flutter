import 'package:ioasys_test/core/di/account_di.dart';
import 'package:ioasys_test/core/di/company_di.dart';
import 'package:ioasys_test/core/di/general_di.dart';

class ExportDi {
  Future<void> initInjection() async {
    await generalDi();
    await accountDi();
    await companyDi();
  }
}
