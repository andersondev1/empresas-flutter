import 'package:get_it/get_it.dart';
import 'package:ioasys_test/accounts/data/datasource/local_datasource.dart';
import 'package:ioasys_test/accounts/data/datasource/remote_datasource.dart';
import 'package:ioasys_test/accounts/data/repositories/account_repository_impl.dart';
import 'package:ioasys_test/accounts/domain/repositories/account_repository.dart';
import 'package:ioasys_test/accounts/domain/usescases/login_account_usecase.dart';

var getIt = GetIt.instance;

Future<void> accountDi() async {
  getIt.registerFactory<LocalDataSource>(
    () => LocalDataSourceImpl(getIt.get()),
  );

  //RemoteDataSource
  getIt.registerFactory<RemoteDataSource>(
    () => RemoteDataSourceImpl(
      getIt.get(),
    ),
  );

  //repository
  getIt.registerFactory<AccountRepository>(
    () => AccountRepositoryImpl(
      getIt.get(),
      getIt.get(),
      getIt.get(),
    ),
  );

  getIt.registerFactory(
    () => LoginAccountUseCase(getIt.get()),
  );
}
