import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get_it/get_it.dart';
import 'package:ioasys_test/core/drivers/connection/check_connection.dart';
import 'package:ioasys_test/core/drivers/dio/dio_client.dart';
import 'package:ioasys_test/core/drivers/localstorage/local_storage.dart';

var getIt = GetIt.I;

Future<void> generalDi() async {
  getIt.registerFactory<DioClient>(
    () => DioClient(getIt.get()),
  );
  getIt.registerFactory(() => Connectivity());
  getIt.registerFactory(() => Connection(getIt.get()));
  getIt.registerFactory<FlutterSecureStorage>(
    () => const FlutterSecureStorage(),
  );
  getIt.registerFactory<ILocalStorage>(
    () => LocalStorage(getIt.get()),
  );
}
