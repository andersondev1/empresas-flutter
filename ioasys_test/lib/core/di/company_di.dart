import 'package:get_it/get_it.dart';
import 'package:ioasys_test/companys/data/datasource/remote_datasource.dart';
import 'package:ioasys_test/companys/data/repositories/enterprises_repository_impl.dart';
import 'package:ioasys_test/companys/domain/repositories/enterprises_repository.dart';
import 'package:ioasys_test/companys/domain/usecases/details_enterprises_usecase.dart';
import 'package:ioasys_test/companys/domain/usecases/search_enterprises_usecase.dart';

var getIt = GetIt.instance;

Future<void> companyDi() async {
  //RemoteDataSource
  getIt.registerFactory<RemoteDataSource>(
    () => RemoteDataSourceImpl(
      getIt.get(),
    ),
  );

  //repository
  getIt.registerFactory<EnterprisesRepository>(
    () => EnterprisesRepositoryImpl(
      getIt.get(),
      getIt.get(),
    ),
  );

  getIt.registerFactory(
    () => SearchEnterprisesUsecase(getIt.get()),
  );
  getIt.registerFactory(() => DetailsEnterprisesUsecase(getIt.get()));
}
