import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:ioasys_test/accounts/presentation/pages/login_account_page.dart';
import 'package:ioasys_test/companys/presentation/pages/company_page.dart';
import 'package:ioasys_test/core/constants/key_routes.dart';
import 'package:ioasys_test/core/constants/key_storage.dart';
import 'package:ioasys_test/core/drivers/localstorage/local_storage.dart';

class MapRoutes {
  MapRoutes() {
    _initialRoute();
  }
  static bool _response = false;
  static void _initialRoute() async {
    LocalStorage localStorage = GetIt.I.get();
    _response = await localStorage.containsKey(KeyStorage.keyHeaders);
    print(_response);
  }

  static Map<String, Widget Function(BuildContext)> map() {
    return {
      KeyRoutes.initialPage: (context) =>
          _response ? const CompanyPage() : const LoginAccountPage(),
      KeyRoutes.homeRoute: (context) => const CompanyPage(),
    };
  }
}
