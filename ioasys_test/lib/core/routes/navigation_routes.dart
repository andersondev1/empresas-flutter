import 'package:flutter/material.dart';

final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

class CustomNavigator {
  Future<void> push(
    String route, {
    Object? arguments,
    bool forRoot = false,
  }) async =>
      await navigatorKey.currentState?.pushNamed(
        route,
        arguments: arguments,
      );

  void pushReplacementNamed(
    String route, {
    Object? arguments,
    bool forRoot = false,
  }) {
    navigatorKey.currentState?.pushReplacementNamed(
      route,
      arguments: arguments,
    );
  }

  void popAndPushNamed(
    String route, {
    Object? arguments,
    bool forRoot = false,
  }) {
    navigatorKey.currentState?.popAndPushNamed(
      route,
      arguments: arguments,
    );
  }

  void pushNamedAndRemoveUntil(
    String route,
    bool Function(Route<dynamic>) predicate, {
    Object? arguments,
  }) {
    navigatorKey.currentState?.pushNamedAndRemoveUntil(route, predicate);
  }

  void pop() => navigatorKey.currentState?.pop();
}
