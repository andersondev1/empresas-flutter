import 'package:dartz/dartz.dart';
import 'package:ioasys_test/companys/domain/entities/cover_interprises_entity.dart';
import 'package:ioasys_test/companys/domain/entities/details_enterprises_entity.dart';
import 'package:ioasys_test/companys/domain/entities/search_enterprises_entity.dart';
import 'package:ioasys_test/core/exceptions/failure.dart';

abstract class EnterprisesRepository {
  Future<Either<Failure, DetailsEnterprisesEntity>> detailsEnterprises(int id);
  Future<Either<Failure, List<CoverEnterprisesEntity>>> searchEnterprises(
    SearchEnterprisesEntity searchEnterprisesEntity,
  );
}
