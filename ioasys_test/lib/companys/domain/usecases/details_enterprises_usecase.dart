import 'package:dartz/dartz.dart';
import 'package:ioasys_test/companys/domain/entities/details_enterprises_entity.dart';
import 'package:ioasys_test/companys/domain/repositories/enterprises_repository.dart';
import 'package:ioasys_test/core/usecases/use_case.dart';

class DetailsEnterprisesUsecase extends UseCase<int, DetailsEnterprisesEntity> {
  final EnterprisesRepository enterprisesRepository;
  DetailsEnterprisesUsecase(this.enterprisesRepository);
  @override
  Future<Either<Exception, DetailsEnterprisesEntity>> call(int params) async {
    return await enterprisesRepository.detailsEnterprises(params);
  }
}
