import 'package:dartz/dartz.dart';
import 'package:ioasys_test/companys/domain/entities/cover_interprises_entity.dart';
import 'package:ioasys_test/companys/domain/entities/search_enterprises_entity.dart';
import 'package:ioasys_test/companys/domain/repositories/enterprises_repository.dart';
import 'package:ioasys_test/core/usecases/use_case.dart';

class SearchEnterprisesUsecase
    extends UseCase<SearchEnterprisesEntity, List<CoverEnterprisesEntity>> {
  final EnterprisesRepository enterprisesRepository;
  SearchEnterprisesUsecase(this.enterprisesRepository);
  @override
  Future<Either<Exception, List<CoverEnterprisesEntity>>> call(
      SearchEnterprisesEntity params) async {
    return await enterprisesRepository.searchEnterprises(params);
  }
}
