class SearchEnterprisesEntity {
  final String name;
  final int id;
  SearchEnterprisesEntity({
    required this.id,
    required this.name,
  });
}
