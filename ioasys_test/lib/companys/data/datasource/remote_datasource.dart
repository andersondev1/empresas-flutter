import 'package:ioasys_test/companys/data/models/cover_interprises_model.dart';
import 'package:ioasys_test/companys/data/models/details_enterprises_model.dart';
import 'package:ioasys_test/companys/data/models/search_enterprises_model.dart';
import 'package:ioasys_test/core/drivers/dio/dio_client.dart';

abstract class RemoteDataSource {
  Future<List<CoverEnterprisesModel>> searchEnterprises(
      SearchEnterprisesModel searchEnterprisesModel);
  Future<DetailsEnterprisesModel> detailsEnterprises(int id);
}

class RemoteDataSourceImpl extends RemoteDataSource {
  final DioClient dioClient;
  RemoteDataSourceImpl(this.dioClient);
  @override
  Future<DetailsEnterprisesModel> detailsEnterprises(int id) async {
    var response = await dioClient.get('/enterprises/$id');
    return DetailsEnterprisesModel.fromJson(response.data);
  }

  @override
  Future<List<CoverEnterprisesModel>> searchEnterprises(
      SearchEnterprisesModel searchEnterprisesModel) async {
    var response = await dioClient.get(
        '/enterprises?enterprise_types=${searchEnterprisesModel.id}/&name=${searchEnterprisesModel.name}');
    return (response.data as List)
        .map((e) => CoverEnterprisesModel.fromJson(response.data))
        .toList();
  }
}
