import 'package:ioasys_test/companys/data/datasource/remote_datasource.dart';
import 'package:ioasys_test/companys/data/models/search_enterprises_model.dart';
import 'package:ioasys_test/companys/domain/entities/search_enterprises_entity.dart';
import 'package:ioasys_test/companys/domain/entities/details_enterprises_entity.dart';
import 'package:ioasys_test/companys/domain/entities/cover_interprises_entity.dart';
import 'package:dartz/dartz.dart';
import 'package:ioasys_test/companys/domain/repositories/enterprises_repository.dart';
import 'package:ioasys_test/core/drivers/connection/check_connection.dart';
import 'package:ioasys_test/core/exceptions/failure.dart';

class EnterprisesRepositoryImpl extends EnterprisesRepository {
  final RemoteDataSource remoteDatasource;
  final Connection connection;
  EnterprisesRepositoryImpl(this.remoteDatasource, this.connection);
  @override
  Future<Either<Failure, DetailsEnterprisesEntity>> detailsEnterprises(
      int id) async {
    if (await connection.checkConnection()) {
      var response = await remoteDatasource.detailsEnterprises(id);
      return Right(response);
    }
    return Left(Failure('no connection'));
  }

  @override
  Future<Either<Failure, List<CoverEnterprisesEntity>>> searchEnterprises(
      SearchEnterprisesEntity searchEnterprisesEntity) async {
    if (await connection.checkConnection()) {
      var response = await remoteDatasource.searchEnterprises(
          SearchEnterprisesModel.fromEntity(searchEnterprisesEntity));
      return Right(response);
    }
    return Left(Failure('no connection'));
  }
}
