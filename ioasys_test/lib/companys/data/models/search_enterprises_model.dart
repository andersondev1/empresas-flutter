import 'package:ioasys_test/companys/domain/entities/search_enterprises_entity.dart';

class SearchEnterprisesModel extends SearchEnterprisesEntity {
  SearchEnterprisesModel({
    required int id,
    required String name,
  }) : super(
          id: id,
          name: name,
        );

  factory SearchEnterprisesModel.fromEntity(
      SearchEnterprisesEntity searchEnterprisesEntity) {
    return SearchEnterprisesModel(
      id: searchEnterprisesEntity.id,
      name: searchEnterprisesEntity.name,
    );
  }
}
