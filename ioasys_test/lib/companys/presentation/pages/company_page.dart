import 'dart:async';

import 'package:flutter/material.dart';
import 'package:ioasys_test/companys/presentation/stores/company_store.dart';
import 'package:provider/provider.dart';

class CompanyPage extends StatefulWidget {
  const CompanyPage({Key? key}) : super(key: key);

  @override
  State<CompanyPage> createState() => _CompanyPageState();
}

class _CompanyPageState extends State<CompanyPage> {
  Timer? _debounce;
  @override
  void dispose() {
    _debounce?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Consumer<CompanyStore>(
      builder: (context, company, _) => Scaffold(
        body: Stack(children: [
          Column(
            children: [
              Container(
                height: size.height * .30,
                color: Colors.amber,
              ),
              Container(
                height: size.height * .70,
                color: Colors.white,
              ),
            ],
          ),
          Positioned(
            top: size.height * .27,
            width: size.width,
            child: TextField(
              style: const TextStyle(
                color: Colors.black,
              ),
              onChanged: getCompany,
              decoration: const InputDecoration(
                prefixIcon: Icon(Icons.search),
                filled: true,
                hintText: 'Pesquisar por empresa',
                focusColor: Colors.grey,
                hoverColor: Colors.grey,
                fillColor: Colors.grey,
                border: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.green),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.green),
                ),
              ),
            ),
          ),
          if (company.listCoverEnterprises.isNotEmpty)
            ListView.builder(
                itemCount: company.listCoverEnterprises.length,
                itemBuilder: (context, index) {
                  return Container();
                })
        ]),
      ),
    );
  }

  void getCompany(String value) {
    if (_debounce?.isActive ?? false) _debounce?.cancel();
    _debounce = Timer(const Duration(milliseconds: 500), () async {});
  }
}
