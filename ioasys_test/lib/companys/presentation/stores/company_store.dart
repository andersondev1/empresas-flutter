import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:ioasys_test/companys/domain/entities/cover_interprises_entity.dart';
import 'package:ioasys_test/companys/domain/entities/details_enterprises_entity.dart';
import 'package:ioasys_test/companys/domain/entities/search_enterprises_entity.dart';
import 'package:ioasys_test/companys/domain/usecases/details_enterprises_usecase.dart';
import 'package:ioasys_test/companys/domain/usecases/search_enterprises_usecase.dart';

enum ViewState { isLoading, error, done }

class CompanyStore extends ChangeNotifier {
  List<CoverEnterprisesEntity> listCoverEnterprises = [];
  DetailsEnterprisesEntity? detailsEnterprisesEntity;
  String error = '';
  void searchCompanys(String name) async {
    SearchEnterprisesUsecase searchEnterprisesUsecase = GetIt.I.get();
    var response = await searchEnterprisesUsecase
        .call(SearchEnterprisesEntity(id: 1, name: name));
    response.fold((l) => error = l.toString(), (result) {
      error = '';
      listCoverEnterprises.addAll(result);
    });
  }

  void detailsCompany(int id) async {
    DetailsEnterprisesUsecase detailsEnterprisesUsecase = GetIt.I.get();
    var response = await detailsEnterprisesUsecase.call(id);
    response.fold(
        (l) => error = l.toString(), (r) => detailsEnterprisesEntity = r);
  }
}
