import 'package:flutter/material.dart';
import 'package:ioasys_test/accounts/presentation/store/login_account_store.dart';
import 'package:ioasys_test/companys/presentation/stores/company_store.dart';
import 'package:ioasys_test/core/di/export_di.dart';
import 'package:ioasys_test/core/routes/map_routes.dart';
import 'package:ioasys_test/core/routes/navigation_routes.dart';
import 'package:provider/provider.dart';

void main() async {
  await ExportDi().initInjection();
  runApp(const AppWidget());
}

class AppWidget extends StatelessWidget {
  const AppWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => LoginAccountStore()),
        ChangeNotifierProvider(create: (context) => CompanyStore()),
      ],
      child: MaterialApp(
        navigatorKey: navigatorKey,
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        initialRoute: '/',
        routes: MapRoutes.map(),
      ),
    );
  }
}
